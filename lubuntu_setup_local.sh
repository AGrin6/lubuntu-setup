sudo dpkg -i ../binaries/Ubuntu-18.04.3/vim-runtime_2%3a8.0.1453-1ubuntu1.4_all.deb
sudo dpkg -i ../binaries/Ubuntu-18.04.3/vim-common_2%3a8.0.1453-1ubuntu1.4_all.deb
sudo dpkg -i ../binaries/Ubuntu-18.04.3/vim_2%3a8.0.1453-1ubuntu1.4_amd64.deb
cp ../../../vim_settings/vimrc /home/lubuntu/.vimrc
mkdir /home/lubuntu/Install
tar xf ../binaries/tor-browser-linux64-10.0.16_en-US.tar.xz -C /home/lubuntu/Install/
tar xf ../binaries/tsetup.2.7.1.tar.xz -C /home/lubuntu/Install/

# git
sudo dpkg -i ../binaries/Ubuntu-18.04.3/liberror-perl_0.17025-1_all.deb
sudo dpkg -i ../binaries/Ubuntu-18.04.3/git-man_1%3a2.17.1-1ubuntu0.8_all.deb
sudo dpkg -i ../binaries/Ubuntu-18.04.3/git_1%3a2.17.1-1ubuntu0.8_amd64.deb
git config --global user.email andrey.grinenko2012@yandex.ru
git config --global user.name Andrei Grinenko

sudo dpkg -i ../binaries/Ubuntu-18.04.3/gstreamer1.0-pulseaudio_1.14.5-0ubuntu1~18.04.2_amd64.deb
sudo dpkg -i ../binaries/Ubuntu-18.04.3/audio-recorder_2.1.3-bionic_amd64.deb

